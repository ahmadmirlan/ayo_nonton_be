import {Category, validate} from '../models/category.model';
import {pick} from 'lodash';
import {buildQuery} from "../middleware/QueryBuilder";

/*
* POST
* Create nwe category
* */
const createCategory = async (req, res) => {
    const body = req.body;
    const {error} = validate(body);
    if (error) return res.status(400).sendError(error.details[0].message);

    try {
        const category = new Category(pick(body, ['name']));
        await category.save();
        return res.send(category);
    } catch (e) {
        return res.status(400).sendError(e);
    }
};

/*
* GET
* Get All categories
* */
const getAllCategories = async (req, res) => {
    try {
        const categories = await Category.find();
        return res.send(categories);
    } catch (e) {
        return res.status(500).sendError(e);
    }
};

/*
* POST
* Find categories
* */
const findCategories = async (req, res) => {
    let query = buildQuery(pick(req.body, [
        'filter',
        'pageNumber',
        'pageSize',
        'sortField',
        'sortOrder',
    ]));
    try {
        let totalElements = await Category.countDocuments(query.filter);
        const categories = await Category.find(query.filter).limit(query.pageSize)
            .skip(query.pageSize * query.pageNumber)
            .sort([[query.sortField, query.sortOrder]]);
        return res.sendData(categories, query.pageSize, totalElements, query.pageNumber);
    } catch (e) {
        return res.status(400).sendError(e);
    }
};

/*
* PUT
* Update category
* */
const updateCategory = async (req, res) => {
    const body = req.body;

    if (!body.id || !body.name) {
        return res.status(400).sendError('Body request invalid (missing parameters id or name)');
    }

    try {
        const category = await Category.findByIdAndUpdate(body.id, {name: body.name}, {new: true});
        if (category) {
            return res.send(category);
        } else {
            return res.status(400).sendError('Invalid body request or invalid provided id category');
        }
    } catch (e) {
        return res.status(500).sendError(e);
    }
};

/*
* DELETE
* Delete category
* */
const deleteCategory = async (req, res) => {
    const {categoryId} = req.params;

    if (!categoryId) {
        return res.status(400).sendError('Please provide category id');
    }

    try {
        Category.findByIdAndDelete(categoryId).then(resp => {
            if (resp) {
                return res.sendMessage(`Category with id ${categoryId} deleted!`);
            }
        });
    } catch (e) {
        return res.status(500).sendError(e);
    }
};

module.exports = {
    createCategory,
    getAllCategories,
    findCategories,
    updateCategory,
    deleteCategory
};
