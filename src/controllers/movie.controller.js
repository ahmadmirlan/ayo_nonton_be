import {Movie, validateMovie} from '../models/movie.model';
import {pick} from 'lodash';
import {buildQuery} from "../middleware/QueryBuilder";

/*
* POST
* Create new movie
* */
const createNewMovie = async (req, res) => {
    const body = req.body;

    const {error} = validateMovie(body);
    if (error) {
        return res.status(400).sendError(error.details[0].message);
    }

    try {
        const movie = new Movie(pick(body, ['title', 'cover', 'actors', 'releaseDate', 'status',
            'trailerURL', 'streamURL', 'description', 'categories', 'rentPrices']));
        // Add signature user
        movie.addedBy = req.user._id;
        await movie.save();
        return res.send(movie);
    } catch (e) {
        return res.status(500).sendError(e);
    }
};

/*
* POST
* Find movies
* */
const findAllMovies = async (req, res) => {
    let query = buildQuery(pick(req.body, [
        'filter',
        'pageNumber',
        'pageSize',
        'sortField',
        'sortOrder',
    ]));
    try {
        let totalElements = await Movie.countDocuments(query.filter);
        const movies = await Movie.find(query.filter)
            .limit(query.pageSize)
            .skip(query.pageSize * query.pageNumber)
            .sort([[query.sortField, query.sortOrder]])
            .populate([{path: 'categories', select: 'name'}]);
        return res.sendData(movies, query.pageSize, totalElements, query.pageNumber);
    } catch (e) {
        return res.status(500).sendError(e);
    }
};

/*
* PUT
* Update movie
* */
const updateMovie = async (req, res) => {
    const {id} = req.body;

    if (!id) {
        return res.status(400).sendError('Please provide movie id!');
    }

    try {
        const movie = await Movie.findByIdAndUpdate(id, pick(req.body, ['title', 'cover', 'actors', 'releaseDate', 'status',
            'trailerURL', 'streamURL', 'description', 'categories', 'rentPrices']), {new: true});
        if (movie) {
            return res.send(movie);
        } else {
            return res.status(400).sendError(`Failed update movie with id ${id}`);
        }
    } catch (e) {
        return res.status(400).sendError(e);
    }
};

/*
* GET
* Find Movie by Id
* */
const findMovieById = async (req, res) => {
    const {movieId} = req.params;

    if (!movieId) {
        return res.status(400).sendError('Please provide movie id!');
    }

    try {
        const movie = await Movie.findById(movieId);
        if (!movie) {
            return res.status(400).sendError("Movie not found");
        }
        return res.send(movie);
    } catch (e) {
        return res.status(500).sendError(e)
    }
};

/*
* POST
* Find All Published Movie
* */
const findAllPublishedMovies = async (req, res) => {
    let query = buildQuery(pick(req.body, [
        'filter',
        'pageNumber',
        'pageSize',
        'sortField',
        'sortOrder',
    ]));

    query.filter['status'] = 'AVAILABLE';

    try {
        let totalElements = await Movie.countDocuments(query.filter);
        const movies = await Movie.find(query.filter)
            .limit(query.pageSize)
            .skip(query.pageSize * query.pageNumber)
            .sort([[query.sortField, query.sortOrder]])
            .populate([{path: 'categories', select: 'name'}]);
        return res.sendData(movies, query.pageSize, totalElements, query.pageNumber);
    } catch (e) {
        return res.status(500).sendError(e);
    }
};

/*
* DELETE
* Delete Movie By Id
* */
const deleteMovieById = async (req, res) => {
    let {movieId} = req.params;

    if (!movieId) {
        return res.status.sendError('Please provide movie id');
    }

    try {
        const movie = await Movie.findOneAndDelete({_id: movieId});
        if (movie) {
            return res.sendMessage(`Movie with id ${movieId} deleted!`);
        } else {
            return res.status(400).sendError(`Failed deleting movie with id ${movieId}`);
        }
    } catch (e) {
        return res.status(400).sendError(e);
    }
};
module.exports = {
    createNewMovie,
    findAllMovies,
    updateMovie,
    findMovieById,
    findAllPublishedMovies,
    deleteMovieById
};
