import AuthController from './auth.controller';
import PermissionsController from './permissions.controller';
import RoleController from './role.controller';
import UserController from './user.controller';
import  CategoryController from './category.controller';
import MovieController from './movie.controller';

module.exports = {
    AuthController,
    PermissionsController,
    RoleController,
    UserController,
    CategoryController,
    MovieController
};
