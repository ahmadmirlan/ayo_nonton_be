import {
    AuthController,
    CategoryController,
    MovieController,
    PermissionsController,
    RoleController,
    UserController
} from '../controllers';
import Auth from '../middleware/permissions/Auth';
import {canDeleteAuth, canEditAuth, canReadAuth} from '../middleware/permissions/auth.guard';
import {canDeleteManagement, canEditManagement, canReadManagement} from '../middleware/permissions/management.guard';

const api = process.env.API_PREFIX;

module.exports = app => {
    /* GET home page. */
    app.get('/', function (req, res, next) {
        res.send({title: `Welcome to ${process.env.PROJECT_NAME} REST API`});
    });

    /*
    * App Version Check
    * */
    app.get(`/${api}/version`, (req, res) => {
        res.send({version: '1.0.0'});
    });

    /*
     * Auth Routes
     * */
    app.post(`/${api}/auth/register`, AuthController.register);
    app.post(`/${api}/auth/login`, AuthController.login);
    app.post(`/${api}/auth/checkUsername`, AuthController.checkUsername);
    app.post(`/${api}/auth/checkEmail`, AuthController.checkEmail);
    app.get(`/${api}/findUser/byToken`, [Auth], AuthController.findUserByToken);

    /*
    * Permissions Routes
    * */
    app.post(`/${api}/permissions/create`, [Auth, canEditAuth], PermissionsController.createPermission);
    app.put(`/${api}/permissions/edit`, [Auth, canEditAuth], PermissionsController.updatePermission);
    app.get(`/${api}/permissions`, PermissionsController.getAllPermissions);

    /*
    * Roles Routes
    * */
    app.post(`/${api}/roles/create`, [Auth, canEditAuth], RoleController.createNewRole);
    app.put(`/${api}/roles/update`, [Auth, canEditAuth], RoleController.updateRole);
    app.get(`/${api}/roles`, RoleController.findAllRoles);
    app.delete(`/${api}/roles/removeRole/:roleId`, [Auth, canDeleteAuth], RoleController.removeRole);
    app.get(`/${api}/roles/findRole/:roleId`, RoleController.findRoleById);

    /*
    * User Routes
    * */
    app.post(`/${api}/users/findAllUsers`, [Auth, canReadAuth], UserController.findAllUsers);
    app.delete(`/${api}/users/removeUser/:userId`, [Auth, canDeleteAuth], UserController.removeUser);

    /*
    * Category Routes
    * */
    app.post(`/${api}/categories/create`, [Auth, canEditManagement], CategoryController.createCategory);
    app.get(`/${api}/categories/getAll`, CategoryController.getAllCategories);
    app.post(`/${api}/categories/findCategories`, [Auth, canReadManagement], CategoryController.findCategories);
    app.put(`/${api}/categories/update`, [Auth, canEditManagement], CategoryController.updateCategory);
    app.delete(`/${api}/categories/delete/:categoryId`, [Auth, canDeleteManagement], CategoryController.deleteCategory);

    /*
    * Movie Routes
    * */
    app.post(`/${api}/movies/create`, [Auth, canEditManagement], MovieController.createNewMovie);
    app.post(`/${api}/movies/findAllMovies`, [Auth, canReadManagement], MovieController.findAllMovies);
    app.post(`/${api}/movies/findAllPublishedMovies`, MovieController.findAllPublishedMovies);
    app.put(`/${api}/movies/update`, [Auth, canReadManagement], MovieController.updateMovie);
    app.get(`/${api}/movies/findById/:movieId`, [Auth, canReadManagement], MovieController.findMovieById);
    app.delete(`/${api}/movies/delete/:movieId`, [Auth, canDeleteManagement], MovieController.deleteMovieById);
};
