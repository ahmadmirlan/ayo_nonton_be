import {model, ObjectId, Schema} from 'mongoose';
import Joi from 'joi';

const movieSchema = new Schema({
    title: {
        type: String,
        minLength: 1,
        maxLength: 250,
        required: true,
        unique: true,
    },
    cover: {
        type: String,
        requires: true
    },
    actors: {
        type: [String],
    },
    releaseDate: {
        type: String,
        required: true,
    },
    status: {
        type: String,
        enum: ['NOT_AVAILABLE', 'AVAILABLE', 'UPCOMING'],
        default: 'NOT_AVAILABLE'
    },
    trailerURL: {
        type: String,
    },
    streamURL: {
        type: String,
    },
    description: {
        type: String
    },
    categories: [{
        type: ObjectId,
        ref: 'Category'
    }],
    rentPrices: {
        type: Number,
    },
    addedBy: {
        type: ObjectId,
        ref: 'User'
    }
}, {
    timestamps: true,
    toJSON: {getters: true},
    toObject: {getters: true},
});

const Movie = model('Movie', movieSchema);

function validateMovie(movie) {
    const schema = {
        title: Joi.string(),
        cover: Joi.string(),
        actors: Joi.array(),
        releaseDate: Joi.string(),
        status: Joi.string(),
        trailerURL: Joi.string(),
        streamURL: Joi.string(),
        description: Joi.string(),
        categories: Joi.array(),
        rentPrices: Joi.number(),
    };
    return Joi.validate(movie, schema);
}

module.exports = {
    Movie,
    validateMovie: validateMovie
};
