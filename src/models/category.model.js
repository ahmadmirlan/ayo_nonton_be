import {model, Schema} from 'mongoose';
import Joi from 'joi';

const categorySchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true,
        minLength: 2
    }
}, {
    timestamps: true,
    toJSON: {getters: true},
    toObject: {getters: true},
});

const Category = model('Category', categorySchema);

function validateCategory(Category) {
    const schema = {
        name: Joi.string().min(2).required(),
    };
    return Joi.validate(Category, schema);
}

module.exports = {
    Category,
    validate: validateCategory
};
