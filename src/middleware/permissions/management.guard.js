import {find} from 'lodash';
import {Role} from '../../models/role.model';

const canReadManagement = async (req, res, next) => {
    // 401 Unauthorized
    // 403 Forbidden
    let role = req.user.role;
    let giveAccess = false;
    let selectedRole = await Role.findById(role).select('permissions').populate([{path: 'permissions', select: 'name'}]);
    let findAccess = find(selectedRole['permissions'], dt => {
        return dt.name === 'canReadManagementModule';
    });
    if (findAccess) {
        giveAccess = true;
    }
    if (!giveAccess) return res.status(403).sendError('Access denied!');
    next();
};

const canEditManagement = async (req, res, next) => {
    // 401 Unauthorized
    // 403 Forbidden
    let role = req.user.role;
    let giveAccess = false;
    let selectedRole = await Role.findById(role).select('permissions').populate([{path: 'permissions', select: 'name'}]);
    let findAccess = find(selectedRole['permissions'], dt => {
        return dt.name === 'canEditManagementModule';
    });
    if (findAccess) {
        giveAccess = true;
    }
    if (!giveAccess) return res.status(403).sendError('Access denied!');
    next();
};

const canDeleteManagement = async (req, res, next) => {
    // 401 Unauthorized
    // 403 Forbidden
    let role = req.user.role;
    let giveAccess = false;
    let selectedRole = await Role.findById(role).select('permissions').populate([{path: 'permissions', select: 'name'}]);
    let findAccess = find(selectedRole['permissions'], dt => {
        return dt.name === 'canDeleteManagementModule';
    });
    if (findAccess) {
        giveAccess = true;
    }
    if (!giveAccess) return res.status(403).sendError('Access denied!');
    next();
};

module.exports = {
    canDeleteManagement,
    canEditManagement,
    canReadManagement,
};
