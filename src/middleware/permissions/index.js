const AdminGuard = require('./admin.guard');
const AuthGuard = require('./auth.guard');
const Auth = require('./Auth');
const ContentGuard = require('./content.guard');
const CoreGuard = require('./core.guard');
const ManagementGuard = require('./management.guard');

module.exports = {
    AdminGuard,
    ContentGuard,
    auth: Auth,
    ManagementGuard,
    AuthGuard,
    CoreGuard
};
