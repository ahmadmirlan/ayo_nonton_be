import {find} from 'lodash';
import {Role} from '../../models/role.model';

const canReadAuth = async (req, res, next) => {
    // 401 Unauthorized
    // 403 Forbidden
    let role = req.user.role;
    let giveAccess = false;
    let selectedRole = await Role.findById(role).select('permissions').populate([{path: 'permissions', select: 'name'}]);
    let findAccess = find(selectedRole['permissions'], dt => {
        return dt.name === 'canReadAuthModule';
    });
    if (findAccess) {
        giveAccess = true;
    }
    if (!giveAccess) return res.status(403).sendError('Access denied!');
    next();
};

const canEditAuth = async (req, res, next) => {
    // 401 Unauthorized
    // 403 Forbidden
    let role = req.user.role;
    let giveAccess = false;
    let selectedRole = await Role.findById(role).select('permissions').populate([{path: 'permissions', select: 'name'}]);
    let findAccess = find(selectedRole['permissions'], dt => {
        return dt.name === 'canEditAuthModule';
    });
    if (findAccess) {
        giveAccess = true;
    }
    if (!giveAccess) return res.status(403).sendError('Access denied!');
    next();
};

const canDeleteAuth = async (req, res, next) => {
    // 401 Unauthorized
    // 403 Forbidden
    let role = req.user.role;
    let giveAccess = false;
    let selectedRole = await Role.findById(role).select('permissions').populate([{path: 'permissions', select: 'name'}]);
    let findAccess = find(selectedRole['permissions'], dt => {
        return dt.name === 'canDeleteAuthModule';
    });
    if (findAccess) {
        giveAccess = true;
    }
    if (!giveAccess) return res.status(403).sendError('Access denied!');
    next();
};

module.exports = {
    canReadAuth,
    canDeleteAuth,
    canEditAuth,
};
